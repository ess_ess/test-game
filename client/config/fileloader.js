import { ASSETS_URL } from '.'

const fileLoader = game => {
  game.load.crossOrigin = 'Anonymous'
  game.stage.backgroundColor = '#1E1E1E'
  game.load.image('asphalt', `${ASSETS_URL}/sprites/asphalt/asphalt.png`)
  game.load.image('car', `${ASSETS_URL}/sprites/car/car.png`)
  game.load.spritesheet('kennewsprites', `${ASSETS_URL}/sprites/car/kennewsprites.png`, 76, 101, 63);
  game.load.spritesheet('mbisonspritesheet', `${ASSETS_URL}/sprites/car/mbisonspritesheet.png`, 65, 95, 48);
}

export default fileLoader
