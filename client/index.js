import { WINDOW_WIDTH, WINDOW_HEIGHT } from './config'
import Game from './state/Game'

class App extends Phaser.Game {
  constructor () {
    super(WINDOW_WIDTH, WINDOW_HEIGHT, Phaser.AUTO)
    this.state.add('Game', Game)
    this.state.start('Game')
  }
}

window.startKen = () => {
  document.getElementById('menu').style.display = 'none'
  window.fighter = 'ken';
  const SimpleGame = new App()
}
window.startBison = () => {
  document.getElementById('menu').style.display = 'none'
  window.fighter = 'bison';
  const SimpleGame = new App()
}
