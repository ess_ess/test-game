import { WORLD_SIZE } from '../config'
import { createText } from './utils'
import fileLoader from '../config/fileloader'
import createWorld from './world/createWorld'
import player from './player'
import newPlayer from './sockets/newPlayer'
import updatePlayers from './sockets/updatePlayers'
import playerMovementInterpolation from './predictions/playerMovementInterpolation'

const SERVER_IP = 'http://localhost:8004/'

let socket = null
let otherPlayers = {}

let cursors
//Punches 
var lightPunchInput;
var mediumPunchInput;
var fiercePunchInput;

//Kicks
var lightKickInput;
var mediumKickInput;
var fierceKickInput;


//Player States;
var kenAttacking;
var kenIdle;
var kenBlocking;
var kenRecovery;
var kenHadouken;

var bisonAttacking;
var bisonIdle;
var bisonBlocking;

//Enemy Code:
var enemyBison;

//Hitbox
var hitBoxes;
var kenHitBox;
var hitbox1;

//collision groups
var kenCollisionGroup;
var mBisonCollisionGroup;


//Player health:
var healthKen;
var healthBison;


//Sizes Ken:
var standingHitBoxWidthKen;
var standingHitBoxHeightKen;
var lightPunchWidthKen;
var mediumPunchWidthKen;

//Sizes Bison:
var standingHitBoxWidthBison;
var standingHitBoxHeightBison;
var lightPunchWidthBison;
var mediumPunchWidhBison;

//Input History: 
var inputString;
var lastLeftInput;
var lastRightInput;
var lastDownInput;
var lastLightPunchInput;
var lastMediumPunchInput;

//fireball:
var bullets;
var bullet;
var nextShot;
nextShot = 0;
    inputString = "";

    healthKen = 100;
    healthBison = 100;

    kenIdle = true;
    kenAttacking = false;
    kenBlocking = false;
    kenHadouken = false;

    bisonIdle = true;
    bisonAttacking = false;
    bisonBlocking = false;

    standingHitBoxWidthKen = 30;
    standingHitBoxHeightKen = 60;
    lightPunchWidthKen = 40;
    mediumPunchWidthKen = 60;

    standingHitBoxWidthBison = 30;
    standingHitBoxHeightBison = 60;
    lightPunchWidthBison = 60;
    mediumPunchWidhBison = 60;

class Game extends Phaser.State {
  constructor () {
    super()
    this.player = {}
    const { fighter } = window;
    this.fighter = fighter === 'bison' ? 'bison' : 'ken';
  }

  preload () {
    // Loads files
    fileLoader(this.game)
  }

  create () {
    console.warn('Create game');
    const { width, height } = WORLD_SIZE
    // Creates the world
    createWorld(this.game)
    // Connects the player to the server
    socket = io(SERVER_IP)

    const { fighter } = this;
    const posY = 400;
    // render bison to right and ken to left
    const posX = fighter === 'bison'
      ? 400
      : 100
    // Creates the player passing the X, Y, game and socket as arguments
    this.player = player(posX, posY, this.game, socket, this.fighter)
    // Creates the player name text
    this.player.playerName = createText(this.game, this.player.sprite.body)
    // Creates the player speed text
    this.player.speedText = createText(this.game, this.player.sprite.body)
    this.player.fighter = this.fighter;

    // Sends a new-player event to the server
    newPlayer(socket, this.player)
    // update all players
    updatePlayers(socket, otherPlayers, this.game)

    // Configures the game camera
    this.game.camera.x = this.player.sprite.x - 800 / 2
    this.game.camera.y = this.player.sprite.y - 600 / 2

    // Scale game to fit the entire window
    this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL

    this.game.physics.startSystem(Phaser.Physics.ARCADE); //game.physics.startSystem(Phaser.Physics.P2JS);
    this.game.physics.arcade.enable(this.player, Phaser.Physics.ARCADE, true);
    this.player.enableBody = true
    this.player.sprite.body.setSize(standingHitBoxWidthKen, standingHitBoxHeightKen);

    this.playerKenSpritesLoad();
    this.inputDeclarations();
  }

  update () {
    this.player.drive(this.game)

    // Move the camera to follow the player
    let cameraX = this.player.sprite.x - 800 / 2
    let cameraY = this.player.sprite.y - 600 / 2
    this.game.camera.x += (cameraX - this.game.camera.x) * 0.08
    this.game.camera.y += (cameraY - this.game.camera.y) * 0.08

    // Interpolates the players movement
    playerMovementInterpolation(otherPlayers)
  }

  playerKenSpritesLoad() {
    //Standing:
    this.player.sprite.animations.add('standing', [0, 1, 2, 3]);
    this.player.sprite.animations.add('walkingforward', [4, 5, 6]);
    this.player.sprite.animations.add('walkingbackward', [8, 9, 10, 11, 12, 13]);

    //Standing Punches:
    this.player.sprite.animations.add('standingLightPunch', [25, 26, 27]);
    this.player.sprite.animations.add('standingMediumPunch', [27, 28, 29, 30]);
    this.player.sprite.animations.add('standingFiercePunch', [31, 32, 33, 34, 35]);

    //jump: 
    this.player.sprite.animations.add('neutralJump', [36, 37, 38, 39]);


    //hadouken
    this.player.sprite.animations.add('hadouken', [42, 43, 44, 45, 44, 43, 42]);


    //light kick:
    this.player.sprite.animations.add('standingLightKick', [46, 47, 48, 49]);
    this.player.sprite.animations.add('standingMediumKick', [50, 51, 52, 53, 54]);
    this.player.sprite.animations.add('standingFierceKick', [55, 56, 57, 58, 59, 60]);

    //got hit standing
    this.player.sprite.animations.add('gotHitStanding', [61, 62, 63]);


    //Crouching:
    this.player.sprite.animations.add('crouching', [14]);
    this.player.sprite.animations.add('crouchingLightPunch', [15, 16, 17]);
    this.player.sprite.animations.add('crouchingMediumPunch', [18, 19, 20, 21, 22, 15]);
    //TODO: add this to the sprite sheet playerKen.animations.add('crouchingFierce',)


    this.player.sprite.animations.add('neutralJump', [36, 37, 38, 39, 40, 41, 42]);
}


inputDeclarations() {
    cursors = this.game.input.keyboard.createCursorKeys();
    lightPunchInput = this.game.input.keyboard.addKey(Phaser.Keyboard.Q);
    mediumPunchInput = this.game.input.keyboard.addKey(Phaser.Keyboard.W);
    fiercePunchInput = this.game.input.keyboard.addKey(Phaser.Keyboard.E);
    lightKickInput = this.game.input.keyboard.addKey(Phaser.Keyboard.A);
    mediumKickInput = this.game.input.keyboard.addKey(Phaser.Keyboard.S);
    fierceKickInput = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
}

}

export default Game
