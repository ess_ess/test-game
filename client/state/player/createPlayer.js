const createPlayer = (x, y, game, fighter) => {
  const spritefile = fighter === 'bison'
    ? 'mbisonspritesheet'
    : 'kennewsprites';
  const sprite =  game.add.sprite(x, y, spritefile);
  game.physics.startSystem(Phaser.Physics.ARCADE);
  game.physics.arcade.enable(sprite,  Phaser.Physics.ARCADE, true)
  //game.physics.p2.enable(sprite, false)
  return sprite
}

export default createPlayer
