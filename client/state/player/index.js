import createPlayer from './createPlayer'
import { isDown } from '../utils'

export default function (x, y, game, socket, fighter) {

  let cursors
//Punches
var lightPunchInput;
var mediumPunchInput;
var fiercePunchInput;

//Kicks
var lightKickInput;
var mediumKickInput;
var fierceKickInput;


//Player States;
var kenAttacking;
var kenIdle;
var kenBlocking;
var kenRecovery;
var kenHadouken;

var bisonAttacking;
var bisonIdle;
var bisonBlocking;

//Enemy Code:
var enemyBison;

//Hitbox
var hitBoxes;
var kenHitBox;
var hitbox1;

//collision groups
var kenCollisionGroup;
var mBisonCollisionGroup;


//Player health:
var healthKen;
var healthBison;


//Sizes Ken:
var standingHitBoxWidthKen;
var standingHitBoxHeightKen;
var lightPunchWidthKen;
var mediumPunchWidthKen;

//Sizes Bison:
var standingHitBoxWidthBison;
var standingHitBoxHeightBison;
var lightPunchWidthBison;
var mediumPunchWidhBison;

//Input History:
var inputString;
var lastLeftInput;
var lastRightInput;
var lastDownInput;
var lastLightPunchInput;
var lastMediumPunchInput;

//fireball:
var bullets;
var bullet;

inputString = "";

healthKen = 100;
healthBison = 100;

kenIdle = true;
kenAttacking = false;
kenBlocking = false;
kenHadouken = false;

bisonIdle = true;
bisonAttacking = false;
bisonBlocking = false;

standingHitBoxWidthKen = 30;
standingHitBoxHeightKen = 60;
lightPunchWidthKen = 40;
mediumPunchWidthKen = 60;

standingHitBoxWidthBison = 30;
standingHitBoxHeightBison = 60;
lightPunchWidthBison = 60;
mediumPunchWidhBison = 60;

  const player = {
    socket,
    sprite: createPlayer(x, y, game, fighter),
    playerName: fighter,
    speed: 0,
    speedText: null,
    drive (game) {
      this.emitPlayerData()
      this.updatePlayerName()

      inputString = ""
      game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
      game.physics.startSystem(Phaser.Physics.ARCADE); //game.physics.startSystem(Phaser.Physics.P2JS);
     // game.physics.arcade.enable(this.sprite, Phaser.Physics.ARCADE, true);
      this.inputDeclarations();
      this.inputHandlers(this.sprite);
      this.inputTracker();
    },

    inputDeclarations() {
      cursors = game.input.keyboard.createCursorKeys();
      lightPunchInput = game.input.keyboard.addKey(Phaser.Keyboard.Q);
      mediumPunchInput = game.input.keyboard.addKey(Phaser.Keyboard.W);
      fiercePunchInput = game.input.keyboard.addKey(Phaser.Keyboard.E);
      lightKickInput = game.input.keyboard.addKey(Phaser.Keyboard.A);
      mediumKickInput = game.input.keyboard.addKey(Phaser.Keyboard.S);
      fierceKickInput = game.input.keyboard.addKey(Phaser.Keyboard.D);
  },
    inputHandlers() {
      //TODO: Neutral jump:
      //   playerKen.body.velocity.y = -50;
      //    playerKen.animations.play('neutralJump', 7, false);
      let action = '';
      if (cursors.left.isDown && cursors.down.isUp) {
          lastLeftInput = game.time.totalElapsedSeconds();
          this.walkLeft();
      } else if (cursors.right.isDown) {
          lastRightInput = game.time.totalElapsedSeconds();
          this.walkRight();
      } else if (cursors.down.isDown) {
          lastDownInput = game.time.totalElapsedSeconds();
          action = [0, 0, 0, 0, 0, 'crouch', 0];
          this.crouch();
      } else if (mediumPunchInput.isDown) {
          action = [3, mediumPunchWidthKen, standingHitBoxHeightKen, 0, 0, 'standingMediumPunch', 10];
          lastMediumPunchInput = game.time.totalElapsedSeconds();
          this.standingHits(...action);
      } else if (lightPunchInput.isDown) {
          action = [3, lightPunchWidthKen, standingHitBoxHeightKen, 0, 0, 'standingLightPunch', 7];
          lastLightPunchInput = game.time.totalElapsedSeconds();
          this.standingHits(...action);

          // we are not doing a hadouken
          // if (inputString.includes("DR[LP]")) {
          //     if ((lastLightPunchInput - lastDownInput) < 0.44425) {
          //         console.log('hadouken');
          //         shootHadouken();
          //     } else {
          //         inputString = "";
          //     }
          // } else {
          //     lastLightPunchInput = game.time.totalElapsedSeconds();
          //     this.standingHits(3, lightPunchWidthKen, standingHitBoxHeightKen, 0, 0, 'standingLightPunch', 7);
          //     console.log('punch');
          // }
      } else if (fiercePunchInput.isDown) {
        // no fiercePunchInput
      } else if (lightKickInput.isDown) {
        action = [5, 60, 60, 0, 0, 'standingLightKick', 12];
        this.standingHits(...action);
      } else if (mediumKickInput.isDown) {
        action = [5, 60, 60, 0, 0, 'standingMediumKick', 12];
        this.standingHits(...action);
      } else if (fierceKickInput.isDown) {
        // no feirce kick for now
        // this.standingHits(5, 65, 60, 0, 0, 'standingFierceKick', 15);
      } else {
        action = [0, 0, 0, 0, 0, 'standing', 0];
        this.standing(this.sprite);
      }
      this.emitPlayerData(action);
  },

    inputTracker() {
      if (cursors.left.downDuration(1)) {
          inputString += "L";
      } else if (cursors.right.downDuration(1)) {
          inputString += "R";
      } else if (cursors.down.downDuration(1)) {
          inputString += "D";
      } else if (mediumPunchInput.downDuration(1)) {
          inputString += "[MP]";
      } else if (lightPunchInput.downDuration(1)) {
          inputString += "[LP]";
      }

  },
   standing(sprite) {
    this.damage = 0;
    if (!kenAttacking) {
      // console.log('standing');
      sprite.body.velocity.x = 0;
      sprite.animations.play('standing', 7, true);
      // console.log('inputStr for standing')
      // console.log(inputString)
    }
  },

   walkLeft() {
    if (!kenAttacking) {
      this.sprite.body.velocity.x = -100;
      this.sprite.animations.play('walkingbackward', 7, true);
    }
  },

   walkRight() {
    if (!kenAttacking) {
        console.log('walkingforward');
        kenIdle = false;
        this.sprite.body.velocity.x = 100;
        this.sprite.animations.play('walkingforward', 7, true);
    }
  },

  crouch() {
    if (cursors && cursors.left.isDown) {
        kenIdle = true;
        kenBlocking = true;
        this.sprite.animations.play('crouching', 7, true);
        this.sprite.body.velocity.x = 0;
    } else {
        if (!kenAttacking && !kenHadouken) {
            kenIdle = true;
            this.sprite.body.velocity.x = 0;
            this.sprite.animations.play('crouching', 7, true);
        }
    }
  },

  standingHits(damage, hitboxWidth, hitboxHeight, offsetX, offsetY, animationPlayString, frameRate) {
    this.damage = damage;
    this.sprite.body.setSize(hitboxWidth, hitboxHeight); //Increase the hitbox.
    this.sprite.body.velocity.x = 0;
    this.sprite.body.static = true;
    kenAttacking = true;
    console.warn(this);
    this.sprite.animations.play(animationPlayString, frameRate, false).onComplete.add(function () {
      this.sprite.body.setSize(standingHitBoxWidthKen, standingHitBoxHeightKen);
        kenAttacking = false;
        this.sprite.body.velocity.x = 0;
        this.sprite.animations.play('standing', 7, true);
        this.sprite.body.static = false;
    }, this);
  },

    emitPlayerData (action) {
      // Emit the 'move-player' event, updating the player's data on the server
      socket.emit('move-player', {
        x: this.sprite.body.x,
        y: this.sprite.body.y,
        angle: this.sprite.body.rotation,
        playerName: {
          name: this.playerName.text,
          x: this.playerName.x,
          y: this.playerName.y
        },
        speed: {
          value: this.speed,
          x: this.speedText.x,
          y: this.speedText.y
        },
        action,
      })
    },
    updatePlayerName (name = this.socket.id, x = this.sprite.body.x - 57, y = this.sprite.body.y - 59) {
      // Updates the player's name text and position
      this.playerName.text = String(name)
      this.playerName.x = x
      this.playerName.y = y
      // Bring the player's name to top
      game.world.bringToTop(this.playerName)
    },
    updatePlayerStatusText (status, x, y, text) {
      // Capitalize the status text
      const capitalizedStatus = status[0].toUpperCase() + status.substring(1)
      let newText = ''
      // Set the speed text to either 0 or the current speed
      this[status] < 0 ? this.newText = 0 : this.newText = this[status]
      // Updates the text position and string
      text.x = x
      text.y = y
      text.text = `${capitalizedStatus}: ${parseInt(this.newText)}`
      game.world.bringToTop(text)
    }
  }
  return player
}
