/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var WINDOW_WIDTH = exports.WINDOW_WIDTH = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
var WINDOW_HEIGHT = exports.WINDOW_HEIGHT = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
var WORLD_SIZE = exports.WORLD_SIZE = { width: 1600, height: 1200 };
var ASSETS_URL = exports.ASSETS_URL = '../assets';

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var isDown = exports.isDown = function isDown(game, key) {
  return game.input.keyboard.isDown(key);
};
var createText = exports.createText = function createText(game, target) {
  return game.add.text(target.x, target.y, '', {
    fontSize: '12px',
    fill: '#FFF',
    align: 'center'
  });
};

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (x, y, game, socket, fighter) {

  var cursors = void 0;
  //Punches
  var lightPunchInput;
  var mediumPunchInput;
  var fiercePunchInput;

  //Kicks
  var lightKickInput;
  var mediumKickInput;
  var fierceKickInput;

  //Player States;
  var kenAttacking;
  var kenIdle;
  var kenBlocking;
  var kenRecovery;
  var kenHadouken;

  var bisonAttacking;
  var bisonIdle;
  var bisonBlocking;

  //Enemy Code:
  var enemyBison;

  //Hitbox
  var hitBoxes;
  var kenHitBox;
  var hitbox1;

  //collision groups
  var kenCollisionGroup;
  var mBisonCollisionGroup;

  //Player health:
  var healthKen;
  var healthBison;

  //Sizes Ken:
  var standingHitBoxWidthKen;
  var standingHitBoxHeightKen;
  var lightPunchWidthKen;
  var mediumPunchWidthKen;

  //Sizes Bison:
  var standingHitBoxWidthBison;
  var standingHitBoxHeightBison;
  var lightPunchWidthBison;
  var mediumPunchWidhBison;

  //Input History:
  var inputString;
  var lastLeftInput;
  var lastRightInput;
  var lastDownInput;
  var lastLightPunchInput;
  var lastMediumPunchInput;

  //fireball:
  var bullets;
  var bullet;

  inputString = "";

  healthKen = 100;
  healthBison = 100;

  kenIdle = true;
  kenAttacking = false;
  kenBlocking = false;
  kenHadouken = false;

  bisonIdle = true;
  bisonAttacking = false;
  bisonBlocking = false;

  standingHitBoxWidthKen = 30;
  standingHitBoxHeightKen = 60;
  lightPunchWidthKen = 40;
  mediumPunchWidthKen = 60;

  standingHitBoxWidthBison = 30;
  standingHitBoxHeightBison = 60;
  lightPunchWidthBison = 60;
  mediumPunchWidhBison = 60;

  var player = {
    socket: socket,
    sprite: (0, _createPlayer2.default)(x, y, game, fighter),
    playerName: fighter,
    speed: 0,
    speedText: null,
    drive: function drive(game) {
      this.emitPlayerData();
      this.updatePlayerName();

      inputString = "";
      game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
      game.physics.startSystem(Phaser.Physics.ARCADE); //game.physics.startSystem(Phaser.Physics.P2JS);
      // game.physics.arcade.enable(this.sprite, Phaser.Physics.ARCADE, true);
      this.inputDeclarations();
      this.inputHandlers(this.sprite);
      this.inputTracker();
    },
    inputDeclarations: function inputDeclarations() {
      cursors = game.input.keyboard.createCursorKeys();
      lightPunchInput = game.input.keyboard.addKey(Phaser.Keyboard.Q);
      mediumPunchInput = game.input.keyboard.addKey(Phaser.Keyboard.W);
      fiercePunchInput = game.input.keyboard.addKey(Phaser.Keyboard.E);
      lightKickInput = game.input.keyboard.addKey(Phaser.Keyboard.A);
      mediumKickInput = game.input.keyboard.addKey(Phaser.Keyboard.S);
      fierceKickInput = game.input.keyboard.addKey(Phaser.Keyboard.D);
    },
    inputHandlers: function inputHandlers() {
      //TODO: Neutral jump:
      //   playerKen.body.velocity.y = -50;
      //    playerKen.animations.play('neutralJump', 7, false);
      var action = '';
      if (cursors.left.isDown && cursors.down.isUp) {
        lastLeftInput = game.time.totalElapsedSeconds();
        this.walkLeft();
      } else if (cursors.right.isDown) {
        lastRightInput = game.time.totalElapsedSeconds();
        this.walkRight();
      } else if (cursors.down.isDown) {
        lastDownInput = game.time.totalElapsedSeconds();
        action = [0, 0, 0, 0, 0, 'crouch', 0];
        this.crouch();
      } else if (mediumPunchInput.isDown) {
        action = [3, mediumPunchWidthKen, standingHitBoxHeightKen, 0, 0, 'standingMediumPunch', 10];
        lastMediumPunchInput = game.time.totalElapsedSeconds();
        this.standingHits.apply(this, _toConsumableArray(action));
      } else if (lightPunchInput.isDown) {
        action = [3, lightPunchWidthKen, standingHitBoxHeightKen, 0, 0, 'standingLightPunch', 7];
        lastLightPunchInput = game.time.totalElapsedSeconds();
        this.standingHits.apply(this, _toConsumableArray(action));

        // we are not doing a hadouken
        // if (inputString.includes("DR[LP]")) {
        //     if ((lastLightPunchInput - lastDownInput) < 0.44425) {
        //         console.log('hadouken');
        //         shootHadouken();
        //     } else {
        //         inputString = "";
        //     }
        // } else {
        //     lastLightPunchInput = game.time.totalElapsedSeconds();
        //     this.standingHits(3, lightPunchWidthKen, standingHitBoxHeightKen, 0, 0, 'standingLightPunch', 7);
        //     console.log('punch');
        // }
      } else if (fiercePunchInput.isDown) {
        // no fiercePunchInput
      } else if (lightKickInput.isDown) {
        action = [5, 60, 60, 0, 0, 'standingLightKick', 12];
        this.standingHits.apply(this, _toConsumableArray(action));
      } else if (mediumKickInput.isDown) {
        action = [5, 60, 60, 0, 0, 'standingMediumKick', 12];
        this.standingHits.apply(this, _toConsumableArray(action));
      } else if (fierceKickInput.isDown) {
        // no feirce kick for now
        // this.standingHits(5, 65, 60, 0, 0, 'standingFierceKick', 15);
      } else {
        action = [0, 0, 0, 0, 0, 'standing', 0];
        this.standing(this.sprite);
      }
      this.emitPlayerData(action);
    },
    inputTracker: function inputTracker() {
      if (cursors.left.downDuration(1)) {
        inputString += "L";
      } else if (cursors.right.downDuration(1)) {
        inputString += "R";
      } else if (cursors.down.downDuration(1)) {
        inputString += "D";
      } else if (mediumPunchInput.downDuration(1)) {
        inputString += "[MP]";
      } else if (lightPunchInput.downDuration(1)) {
        inputString += "[LP]";
      }
    },
    standing: function standing(sprite) {
      this.damage = 0;
      if (!kenAttacking) {
        // console.log('standing');
        sprite.body.velocity.x = 0;
        sprite.animations.play('standing', 7, true);
        // console.log('inputStr for standing')
        // console.log(inputString)
      }
    },
    walkLeft: function walkLeft() {
      if (!kenAttacking) {
        this.sprite.body.velocity.x = -100;
        this.sprite.animations.play('walkingbackward', 7, true);
      }
    },
    walkRight: function walkRight() {
      if (!kenAttacking) {
        console.log('walkingforward');
        kenIdle = false;
        this.sprite.body.velocity.x = 100;
        this.sprite.animations.play('walkingforward', 7, true);
      }
    },
    crouch: function crouch() {
      if (cursors && cursors.left.isDown) {
        kenIdle = true;
        kenBlocking = true;
        this.sprite.animations.play('crouching', 7, true);
        this.sprite.body.velocity.x = 0;
      } else {
        if (!kenAttacking && !kenHadouken) {
          kenIdle = true;
          this.sprite.body.velocity.x = 0;
          this.sprite.animations.play('crouching', 7, true);
        }
      }
    },
    standingHits: function standingHits(damage, hitboxWidth, hitboxHeight, offsetX, offsetY, animationPlayString, frameRate) {
      this.damage = damage;
      this.sprite.body.setSize(hitboxWidth, hitboxHeight); //Increase the hitbox.
      this.sprite.body.velocity.x = 0;
      this.sprite.body.static = true;
      kenAttacking = true;
      console.warn(this);
      this.sprite.animations.play(animationPlayString, frameRate, false).onComplete.add(function () {
        this.sprite.body.setSize(standingHitBoxWidthKen, standingHitBoxHeightKen);
        kenAttacking = false;
        this.sprite.body.velocity.x = 0;
        this.sprite.animations.play('standing', 7, true);
        this.sprite.body.static = false;
      }, this);
    },
    emitPlayerData: function emitPlayerData(action) {
      // Emit the 'move-player' event, updating the player's data on the server
      socket.emit('move-player', {
        x: this.sprite.body.x,
        y: this.sprite.body.y,
        angle: this.sprite.body.rotation,
        playerName: {
          name: this.playerName.text,
          x: this.playerName.x,
          y: this.playerName.y
        },
        speed: {
          value: this.speed,
          x: this.speedText.x,
          y: this.speedText.y
        },
        action: action
      });
    },
    updatePlayerName: function updatePlayerName() {
      var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.socket.id;
      var x = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.sprite.body.x - 57;
      var y = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : this.sprite.body.y - 59;

      // Updates the player's name text and position
      this.playerName.text = String(name);
      this.playerName.x = x;
      this.playerName.y = y;
      // Bring the player's name to top
      game.world.bringToTop(this.playerName);
    },
    updatePlayerStatusText: function updatePlayerStatusText(status, x, y, text) {
      // Capitalize the status text
      var capitalizedStatus = status[0].toUpperCase() + status.substring(1);
      var newText = '';
      // Set the speed text to either 0 or the current speed
      this[status] < 0 ? this.newText = 0 : this.newText = this[status];
      // Updates the text position and string
      text.x = x;
      text.y = y;
      text.text = capitalizedStatus + ': ' + parseInt(this.newText);
      game.world.bringToTop(text);
    }
  };
  return player;
};

var _createPlayer = __webpack_require__(7);

var _createPlayer2 = _interopRequireDefault(_createPlayer);

var _utils = __webpack_require__(1);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _config = __webpack_require__(0);

var _Game = __webpack_require__(4);

var _Game2 = _interopRequireDefault(_Game);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var App = function (_Phaser$Game) {
  _inherits(App, _Phaser$Game);

  function App() {
    _classCallCheck(this, App);

    var _this = _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this, _config.WINDOW_WIDTH, _config.WINDOW_HEIGHT, Phaser.AUTO));

    _this.state.add('Game', _Game2.default);
    _this.state.start('Game');
    return _this;
  }

  return App;
}(Phaser.Game);

window.startKen = function () {
  document.getElementById('menu').style.display = 'none';
  window.fighter = 'ken';
  var SimpleGame = new App();
};
window.startBison = function () {
  document.getElementById('menu').style.display = 'none';
  window.fighter = 'bison';
  var SimpleGame = new App();
};

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _config = __webpack_require__(0);

var _utils = __webpack_require__(1);

var _fileloader = __webpack_require__(5);

var _fileloader2 = _interopRequireDefault(_fileloader);

var _createWorld = __webpack_require__(6);

var _createWorld2 = _interopRequireDefault(_createWorld);

var _player = __webpack_require__(2);

var _player2 = _interopRequireDefault(_player);

var _newPlayer = __webpack_require__(8);

var _newPlayer2 = _interopRequireDefault(_newPlayer);

var _updatePlayers = __webpack_require__(9);

var _updatePlayers2 = _interopRequireDefault(_updatePlayers);

var _playerMovementInterpolation = __webpack_require__(10);

var _playerMovementInterpolation2 = _interopRequireDefault(_playerMovementInterpolation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SERVER_IP = 'http://localhost:8004/';

var socket = null;
var otherPlayers = {};

var cursors = void 0;
//Punches 
var lightPunchInput;
var mediumPunchInput;
var fiercePunchInput;

//Kicks
var lightKickInput;
var mediumKickInput;
var fierceKickInput;

//Player States;
var kenAttacking;
var kenIdle;
var kenBlocking;
var kenRecovery;
var kenHadouken;

var bisonAttacking;
var bisonIdle;
var bisonBlocking;

//Enemy Code:
var enemyBison;

//Hitbox
var hitBoxes;
var kenHitBox;
var hitbox1;

//collision groups
var kenCollisionGroup;
var mBisonCollisionGroup;

//Player health:
var healthKen;
var healthBison;

//Sizes Ken:
var standingHitBoxWidthKen;
var standingHitBoxHeightKen;
var lightPunchWidthKen;
var mediumPunchWidthKen;

//Sizes Bison:
var standingHitBoxWidthBison;
var standingHitBoxHeightBison;
var lightPunchWidthBison;
var mediumPunchWidhBison;

//Input History: 
var inputString;
var lastLeftInput;
var lastRightInput;
var lastDownInput;
var lastLightPunchInput;
var lastMediumPunchInput;

//fireball:
var bullets;
var bullet;
var nextShot;
nextShot = 0;
inputString = "";

healthKen = 100;
healthBison = 100;

kenIdle = true;
kenAttacking = false;
kenBlocking = false;
kenHadouken = false;

bisonIdle = true;
bisonAttacking = false;
bisonBlocking = false;

standingHitBoxWidthKen = 30;
standingHitBoxHeightKen = 60;
lightPunchWidthKen = 40;
mediumPunchWidthKen = 60;

standingHitBoxWidthBison = 30;
standingHitBoxHeightBison = 60;
lightPunchWidthBison = 60;
mediumPunchWidhBison = 60;

var Game = function (_Phaser$State) {
  _inherits(Game, _Phaser$State);

  function Game() {
    _classCallCheck(this, Game);

    var _this = _possibleConstructorReturn(this, (Game.__proto__ || Object.getPrototypeOf(Game)).call(this));

    _this.player = {};
    var _window = window,
        fighter = _window.fighter;

    _this.fighter = fighter === 'bison' ? 'bison' : 'ken';
    return _this;
  }

  _createClass(Game, [{
    key: 'preload',
    value: function preload() {
      // Loads files
      (0, _fileloader2.default)(this.game);
    }
  }, {
    key: 'create',
    value: function create() {
      console.warn('Create game');
      var width = _config.WORLD_SIZE.width,
          height = _config.WORLD_SIZE.height;
      // Creates the world

      (0, _createWorld2.default)(this.game);
      // Connects the player to the server
      socket = io(SERVER_IP);

      var fighter = this.fighter;

      var posY = 400;
      // render bison to right and ken to left
      var posX = fighter === 'bison' ? 400 : 100;
      // Creates the player passing the X, Y, game and socket as arguments
      this.player = (0, _player2.default)(posX, posY, this.game, socket, this.fighter);
      // Creates the player name text
      this.player.playerName = (0, _utils.createText)(this.game, this.player.sprite.body);
      // Creates the player speed text
      this.player.speedText = (0, _utils.createText)(this.game, this.player.sprite.body);
      this.player.fighter = this.fighter;

      // Sends a new-player event to the server
      (0, _newPlayer2.default)(socket, this.player);
      // update all players
      (0, _updatePlayers2.default)(socket, otherPlayers, this.game);

      // Configures the game camera
      this.game.camera.x = this.player.sprite.x - 800 / 2;
      this.game.camera.y = this.player.sprite.y - 600 / 2;

      // Scale game to fit the entire window
      this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

      this.game.physics.startSystem(Phaser.Physics.ARCADE); //game.physics.startSystem(Phaser.Physics.P2JS);
      this.game.physics.arcade.enable(this.player, Phaser.Physics.ARCADE, true);
      this.player.enableBody = true;
      this.player.sprite.body.setSize(standingHitBoxWidthKen, standingHitBoxHeightKen);

      this.playerKenSpritesLoad();
      this.inputDeclarations();
    }
  }, {
    key: 'update',
    value: function update() {
      this.player.drive(this.game);

      // Move the camera to follow the player
      var cameraX = this.player.sprite.x - 800 / 2;
      var cameraY = this.player.sprite.y - 600 / 2;
      this.game.camera.x += (cameraX - this.game.camera.x) * 0.08;
      this.game.camera.y += (cameraY - this.game.camera.y) * 0.08;

      // Interpolates the players movement
      (0, _playerMovementInterpolation2.default)(otherPlayers);
    }
  }, {
    key: 'playerKenSpritesLoad',
    value: function playerKenSpritesLoad() {
      //Standing:
      this.player.sprite.animations.add('standing', [0, 1, 2, 3]);
      this.player.sprite.animations.add('walkingforward', [4, 5, 6]);
      this.player.sprite.animations.add('walkingbackward', [8, 9, 10, 11, 12, 13]);

      //Standing Punches:
      this.player.sprite.animations.add('standingLightPunch', [25, 26, 27]);
      this.player.sprite.animations.add('standingMediumPunch', [27, 28, 29, 30]);
      this.player.sprite.animations.add('standingFiercePunch', [31, 32, 33, 34, 35]);

      //jump: 
      this.player.sprite.animations.add('neutralJump', [36, 37, 38, 39]);

      //hadouken
      this.player.sprite.animations.add('hadouken', [42, 43, 44, 45, 44, 43, 42]);

      //light kick:
      this.player.sprite.animations.add('standingLightKick', [46, 47, 48, 49]);
      this.player.sprite.animations.add('standingMediumKick', [50, 51, 52, 53, 54]);
      this.player.sprite.animations.add('standingFierceKick', [55, 56, 57, 58, 59, 60]);

      //got hit standing
      this.player.sprite.animations.add('gotHitStanding', [61, 62, 63]);

      //Crouching:
      this.player.sprite.animations.add('crouching', [14]);
      this.player.sprite.animations.add('crouchingLightPunch', [15, 16, 17]);
      this.player.sprite.animations.add('crouchingMediumPunch', [18, 19, 20, 21, 22, 15]);
      //TODO: add this to the sprite sheet playerKen.animations.add('crouchingFierce',)


      this.player.sprite.animations.add('neutralJump', [36, 37, 38, 39, 40, 41, 42]);
    }
  }, {
    key: 'inputDeclarations',
    value: function inputDeclarations() {
      cursors = this.game.input.keyboard.createCursorKeys();
      lightPunchInput = this.game.input.keyboard.addKey(Phaser.Keyboard.Q);
      mediumPunchInput = this.game.input.keyboard.addKey(Phaser.Keyboard.W);
      fiercePunchInput = this.game.input.keyboard.addKey(Phaser.Keyboard.E);
      lightKickInput = this.game.input.keyboard.addKey(Phaser.Keyboard.A);
      mediumKickInput = this.game.input.keyboard.addKey(Phaser.Keyboard.S);
      fierceKickInput = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
    }
  }]);

  return Game;
}(Phaser.State);

exports.default = Game;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ = __webpack_require__(0);

var fileLoader = function fileLoader(game) {
  game.load.crossOrigin = 'Anonymous';
  game.stage.backgroundColor = '#1E1E1E';
  game.load.image('asphalt', _.ASSETS_URL + '/sprites/asphalt/asphalt.png');
  game.load.image('car', _.ASSETS_URL + '/sprites/car/car.png');
  game.load.spritesheet('kennewsprites', _.ASSETS_URL + '/sprites/car/kennewsprites.png', 76, 101, 63);
  game.load.spritesheet('mbisonspritesheet', _.ASSETS_URL + '/sprites/car/mbisonspritesheet.png', 65, 95, 48);
};

exports.default = fileLoader;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = __webpack_require__(0);

var width = _config.WORLD_SIZE.width,
    height = _config.WORLD_SIZE.height;


var worldCreator = function worldCreator(game) {
  // Start P2 physics engine
  game.physics.startSystem(Phaser.Physics.P2JS);
  // We set this to true so our game won't pause if we focus
  // something else other than the browser
  game.stage.disableVisibilityChange = true;
  // Here we set the bounds of our game world
  game.world.setBounds(0, 0, width, height);
  createMap(game);
};

var createMap = function createMap(game) {
  var groundTiles = [];
  for (var i = 0; i <= width / 64 + 1; i++) {
    for (var j = 0; j <= height / 64 + 1; j++) {
      var groundSprite = game.add.sprite(i * 64, j * 64, 'asphalt');
      groundTiles.push(groundSprite);
    }
  }
};

exports.default = worldCreator;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var createPlayer = function createPlayer(x, y, game, fighter) {
  var spritefile = fighter === 'bison' ? 'mbisonspritesheet' : 'kennewsprites';
  var sprite = game.add.sprite(x, y, spritefile);
  game.physics.startSystem(Phaser.Physics.ARCADE);
  game.physics.arcade.enable(sprite, Phaser.Physics.ARCADE, true);
  //game.physics.p2.enable(sprite, false)
  return sprite;
};

exports.default = createPlayer;

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var newPlayer = function newPlayer(socket, player) {
  socket.on('connect', function () {
    socket.emit('new-player', {
      x: player.sprite.body.x,
      y: player.sprite.body.y,
      angle: player.sprite.rotation,
      fighter: player.fighter,
      playerName: {
        name: String(socket.id),
        x: player.playerName.x,
        y: player.playerName.y
      },
      speed: {
        value: player.speed,
        x: player.speed.x,
        y: player.speed.y
      }
    });
  });
};

exports.default = newPlayer;

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _player = __webpack_require__(2);

var _player2 = _interopRequireDefault(_player);

var _utils = __webpack_require__(1);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var MAX_PLAYERS = 2;
var updatePlayers = function updatePlayers(socket, otherPlayers, game) {
  socket.on('update-players', function (playersData) {
    var playersFound = {};
    // Iterate over all players
    for (var index in playersData) {
      console.log('players ', Object.keys(playersData).length);
      var data = playersData[index];
      /*if(Object.keys(playersData).length > MAX_PLAYERS) {
        console.log('player exceede')
        return
      }*/

      // In case a player hasn't been created yet
      // We make sure that we won't create a second instance of it
      if (otherPlayers[index] === undefined && index !== socket.id) {
        var newPlayer = (0, _player2.default)(data.x, data.y, game, null, data.fighter);
        newPlayer.playerName = (0, _utils.createText)(game, newPlayer);
        newPlayer.speedText = (0, _utils.createText)(game, newPlayer);
        newPlayer.updatePlayerName(data.playerName.name, data.playerName.x, data.playerName.y);
        otherPlayers[index] = newPlayer;
      }

      playersFound[index] = true;

      // Update players data
      if (index !== socket.id) {
        var action = data.action;
        // Update players target but not their real position
        otherPlayers[index].target_x = data.x;
        otherPlayers[index].target_y = data.y;
        otherPlayers[index].target_rotation = data.angle;

        otherPlayers[index].playerName.target_x = data.playerName.x;
        otherPlayers[index].playerName.target_y = data.playerName.y;

        otherPlayers[index].speedText.target_x = data.speed.x;
        otherPlayers[index].speedText.target_y = data.speed.y;

        otherPlayers[index].speed = data.speed.value;
        var hits = ['standingMediumPunch', 'standingLightPunch', 'standingLightKick', 'standingMediumKick'];
        var actionType = action && action[5];
        try {
          if (hits.includes(actionType)) {
            var _otherPlayers$index$s;

            (_otherPlayers$index$s = otherPlayers[index].standingHits).call.apply(_otherPlayers$index$s, [otherPlayers[index]].concat(_toConsumableArray(action)));
          }
          if (actionType === 'standing') {
            console.warn('standing');
            otherPlayers[index].standing.call(otherPlayers[index], otherPlayers[index].sprite);
          }
          if (actionType === 'crouch') {
            otherPlayers[index].crouch.call(otherPlayers[index], otherPlayers[index].sprite);
          }
        } catch (error) {
          console.warn(error);
        }
      }
    }

    // Check if there's no missing players, if there is, delete them
    for (var id in otherPlayers) {
      if (!playersFound[id]) {
        otherPlayers[id].sprite.destroy();
        otherPlayers[id].playerName.destroy();
        otherPlayers[id].speedText.destroy();
        delete otherPlayers[id];
      }
    }
  });
};

exports.default = updatePlayers;

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var playerMovementInterpolation = function playerMovementInterpolation(otherPlayers) {
  for (var id in otherPlayers) {
    var player = otherPlayers[id];
    if (player.target_x !== undefined) {
      // Interpolate the player's position
      player.sprite.body.x += (player.target_x - player.sprite.body.x) * 0.30;
      player.sprite.body.y += (player.target_y - player.sprite.body.y) * 0.30;

      var angle = player.target_rotation;
      var direction = (angle - player.sprite.body.rotation) / (Math.PI * 2);
      direction -= Math.round(direction);
      direction *= Math.PI * 2;
      player.sprite.body.rotation += direction * 0.30;

      // Interpolate the player's name position
      player.playerName.x += (player.playerName.target_x - player.playerName.x) * 0.30;
      player.playerName.y += (player.playerName.target_y - player.playerName.y) * 0.30;

      // Interpolate the player's speed text position
      player.speedText.x += (player.speedText.target_x - player.speedText.x) * 0.30;
      player.speedText.y += (player.speedText.target_y - player.speedText.y) * 0.30;

      player.updatePlayerStatusText('speed', player.speedText.x, player.speedText.y, player.speedText);
    }
  }
};

exports.default = playerMovementInterpolation;

/***/ })
/******/ ]);